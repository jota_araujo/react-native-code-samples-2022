import React, { memo, useCallback, useState } from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import {
  Path as BasePath,
  AreaChart,
  AnimatedPathProps,
} from "react-native-svg-charts";
import * as shape from "d3-shape";
import { Defs, LinearGradient, Stop } from "react-native-svg";

import { theme } from "../constants/";
import chartData from "../data/chartData";

import Box, { VStack } from "./Box";
import TimeframeSelector, { TimeframeButton } from "./TimeframeSelector";

// NOTE: This hack is a fix for bad type definition on this package
interface PathProps extends AnimatedPathProps {
  d?: string;
}
const Path = BasePath as React.ComponentType<PathProps>;

export const { width: screenW, height: screenH } = Dimensions.get("window");

const TIMEFRAMES = ["1D", "5D", "1M", "1Y", "All"];

interface PriceChartProps {
  currency: string;
}

export default memo(function PriceChart({ currency }: PriceChartProps) {
  const [data, setData] = useState(chartData["1D"]);
  const [xPos, setXPos] = useState(0);
  const [selectedTimeframe, setSelectedTimeframe] = useState("1D");

  const onTimeframeSelected = useCallback((label: string, xPos: number) => {
    setXPos(xPos);
    setData(chartData[label]);
    setSelectedTimeframe(label);
  }, []);

  const Gradient = ({ index }: { index: string }) => (
    <Defs key={index}>
      <LinearGradient id={"gradient"} x1={"0%"} y1={"0%"} x2={"0%"} y2={"100%"}>
        <Stop offset={"0%"} stopColor={theme.colors.green} stopOpacity={1} />
        <Stop offset={"90%"} stopColor={theme.colors.green} stopOpacity={0} />
      </LinearGradient>
    </Defs>
  );

  const Line = ({ line }: { line?: string }) => (
    <Path d={line as string} stroke={theme.colors.accentGreen} fill={"none"} />
  );

  const Shadow = ({ line }: { line?: string }) => (
    <Path
      key={"shadow"}
      y={5}
      d={line}
      fill={"none"}
      strokeWidth={4}
      stroke={"rgb(148, 230, 198, 0.7)"}
    />
  );

  return (
    <VStack p={4} full>
      <Box hz spaceAround center>
        <TimeframeSelector xOffset={xPos} />
        {TIMEFRAMES.map((label, index) => (
          <TimeframeButton
            key={index}
            label={label}
            currentTimeframe={selectedTimeframe}
            onPress={onTimeframeSelected}
          />
        ))}
      </Box>
      <View style={{ height: 200 }}>
        <AreaChart
          style={{ height: 200 }}
          curve={shape.curveNatural}
          data={data}
          animate={true}
          svg={{ fill: "url(#gradient)" }}
          contentInset={{ top: 30, bottom: 30 }}
        >
          <Shadow />
          <Line />
          <Gradient />
        </AreaChart>
      </View>
    </VStack>
  );
});

const styles = StyleSheet.create({
  timeLabel: {
    color: theme.colors.midGray,
    fontSize: theme.typography.title.small,
    fontFamily: theme.typography.title.bold,
  },
  title: {
    fontFamily: theme.typography.title.bold,
    fontSize: 16,
    paddingTop: theme.spacing.normal,
    color: theme.colors.lightGray,
    textAlignVertical: "center",
  },
});
