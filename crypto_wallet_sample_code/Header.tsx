import { memo } from "react";
import { Image, Pressable, StyleSheet, Text } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { theme } from "../constants/";
import Icon from "./Icon";
import Box from "./Box";
import NotificationWidget from "./NotificationWidget";

interface HeaderProps {
  headerRightClick: () => void;
  headerLeftClick: () => void;
  title: string;
  hasNotification: boolean;
}

export default memo(function Header({
  title,
  headerLeftClick,
  headerRightClick,
  hasNotification,
}: HeaderProps) {
  return (
    <Box hz full spaceBtw h={11} px={theme.padding.small}>
      <Pressable onPress={headerLeftClick}>
        <Icon>
          <FontAwesome5 name="arrow-left" size={28} color="#eee" />
        </Icon>
      </Pressable>
      {!!title.length && <Text style={styles.title}>{title}</Text>}
      <Pressable onPress={headerRightClick}>
        <Icon>
          <Ionicons name="notifications" size={28} color="#eee" />
          {hasNotification && <NotificationWidget />}
        </Icon>
      </Pressable>
    </Box>
  );
});

const styles = StyleSheet.create({
  title: {
    fontFamily: theme.typography.title.bold,
    fontSize: theme.typography.title.small,
    paddingTop: theme.spacing.normal,
    color: theme.colors.lightGray,
    textAlignVertical: "center",
  },
});
