import React, { memo, useEffect, useState } from "react";
import { Pressable, StyleSheet } from "react-native";
import Animated, {
  interpolateColor,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  withTiming,
} from "react-native-reanimated";

import { useAnimatedTransform } from "../hooks";
import { theme } from "../constants/";

import Box from "./Box";

const BUTTON_SIZE = 36;

interface TimeframeSelectorProps {
  xOffset: number;
}

export default memo(function TimeframeSelector({
  xOffset,
}: TimeframeSelectorProps) {
  const { animation: offset, style: offsetStyle } = useAnimatedTransform(
    "translateX",
    0
  );

  useEffect(() => {
    offset.value = withSpring(xOffset, { overshootClamping: true });
  }, [xOffset]);

  return (
    <Box
      animated
      absolute
      center
      p={2}
      w={6}
      h={6}
      bg={theme.colors.lightPurple}
      br={6}
      style={offsetStyle}
    />
  );
});

interface TimeframeButtonProps {
  label: string;
  currentTimeframe: string;
  onPress: (label: string, xPos: number) => void;
}

export const TimeframeButton = memo(
  ({ currentTimeframe, label, onPress }: TimeframeButtonProps) => {
    const [xPos, setXPos] = useState(0);
    const progress = useSharedValue<number>(0);

    const colorStyle = useAnimatedStyle(() => {
      return {
        color: interpolateColor(
          progress.value,
          [0, 1],
          [theme.colors.lightGray, theme.colors.white]
        ),
      };
    });

    useEffect(() => {
      const active = currentTimeframe === label;
      const targetValue = active ? 1 : 0;
      progress.value = withTiming(targetValue, { duration: 300 });
    }, [currentTimeframe]);

    return (
      <Pressable
        onLayout={(event) => {
          const layout = event.nativeEvent.layout;
          setXPos(layout.x - BUTTON_SIZE / 2);
        }}
        onPress={() => onPress(label, xPos)}
      >
        <Box>
          <Animated.Text style={[styles.timeLabel, colorStyle]}>
            {label}
          </Animated.Text>
        </Box>
      </Pressable>
    );
  }
);

const styles = StyleSheet.create({
  timeLabel: {
    color: theme.colors.midGray,
    fontSize: theme.typography.title.small,
    fontFamily: theme.typography.title.bold,
  },
});
