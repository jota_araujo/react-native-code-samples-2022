import { View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import Animated, {
  Easing,
  EasingNode,
  useDerivedValue,
  useSharedValue,
  withDelay,
  withTiming,
} from "react-native-reanimated";
import React, { memo, useEffect, useState } from "react";
import Checkbox from "./Checkbox";
import Ripple from "./Ripple";
import { useAnimatedProgress, useAnimatedTransform } from "./hooks";
import { theme, st, screenW, screenH } from "../constants";
import { Event } from "/types";

import { styles, TILE_SIZE, PADDING } from "./styles";

//const PADDING = 10;

const TILE_ANIMATION_CONFIG = {
  duration: 600,
  easing: Easing.out(Easing.quad),
};
const TEXT_ANIMATION_CONFIG = {
  duration: 400,
};

interface StaticTileProps {
  id: string;
  x: number;
  y: number;
  colors: string[];
  lit: boolean;
}

interface TileProps extends StaticTileProps {
  event: Event;
  open: boolean;
  displace: boolean;
}

export const StaticTile = memo(function _Tile({
  id,
  x,
  y,
  colors,
  lit,
}: StaticTileProps) {
  return (
    <View
      style={[
        styles.tile,
        {
          opacity: lit ? 1 : 0.15,
          left: x,
          top: y,
        },
      ]}
    >
      <LinearGradient
        start={[0, 0]}
        end={[1, 0]}
        colors={colors}
        style={styles.gradient}
      />
    </View>
  );
});

export const Tile = memo(function _Tile({
  id,
  x,
  y,
  lit,
  colors,
  event,
  displace,
  open,
}: TileProps) {
  const { animation: xOffset, style: offsetStyle } = useAnimatedTransform(
    "translateX",
    0
  );

  const { progress: widthProgress, style: sizeStyle } = useAnimatedProgress(
    "width",
    0,
    TILE_SIZE,
    TILE_SIZE
  );

  const { progress: opacityProgress, style: opacityStyle } =
    useAnimatedProgress("opacity", 0);
  const textOpacity = useSharedValue(0);
  const [checked, setChecked] = useState<boolean>(false);

  useEffect(() => {
    const targetValue = displace ? TILE_SIZE : 0;
    xOffset.value = withTiming(targetValue, TILE_ANIMATION_CONFIG);
  }, [displace]);

  useEffect(() => {
    const targetValue = open ? 1 : 0;
    widthProgress.value = withTiming(targetValue, TILE_ANIMATION_CONFIG);
    opacityProgress.value = withDelay(
      350,
      withTiming(targetValue, TEXT_ANIMATION_CONFIG)
    );
  }, [open]);

  return (
    <Animated.View
      style={[
        styles.tile,
        { left: x, top: y, opacity: lit ? 1 : 0.15 },
        offsetStyle,
        sizeStyle,
      ]}
    >
      <Ripple
        style={styles.ripple}
        onTap={() => {
          setChecked((prev) => !prev);
        }}
      >
        <LinearGradient
          start={[0, 0]}
          end={[1, 0]}
          colors={lit ? event.color : colors}
          style={styles.gradient}
        >
          <Animated.Text
            style={[styles.text, checked ? styles.strike : {}, opacityStyle]}
          >
            {event.title}
          </Animated.Text>
          <Animated.Text style={[styles.detail, opacityStyle]}>
            {event.detail}
          </Animated.Text>
          {!!lit && <Checkbox checked={checked} />}
        </LinearGradient>
      </Ripple>
    </Animated.View>
  );
});
