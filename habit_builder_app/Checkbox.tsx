import { memo, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import { theme, st } from "../constants";
import { useAnimatedProgress, useAnimatedTransform } from "./hooks";
import Animated, { withRepeat, withTiming } from "react-native-reanimated";

export default memo(function Checkbox({ checked }: { checked: boolean }) {
  const { animation: scaleAnimation, style: scaledStyle } =
    useAnimatedTransform("scale", 1);

  const { progress: opacityProgress, style: animatedOpacity } =
    useAnimatedProgress("opacity", 0);
  useEffect(() => {
    scaleAnimation.value = withRepeat(withTiming(2), 2, true);
    opacityProgress.value = withTiming(checked ? 1 : 0, { duration: 400 });
  }, [checked]);

  return (
    <View style={styles.box}>
      <Animated.View style={[scaledStyle, animatedOpacity]}>
        <FontAwesome name="check" size={24} color="white" />
      </Animated.View>
    </View>
  );
});

const styles = StyleSheet.create({
  box: {
    position: "absolute",
    borderRadius: 10,
    padding: 8,
    justifyContent: "center",
    alignItems: "center",
    bottom: 15,
    right: 15,
    backgroundColor: "#00000018",
  },
});
