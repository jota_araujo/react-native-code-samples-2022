import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import { registerRootComponent } from "expo";
import Slider from "./components/Slider";
import Calendar from "./components/Calendar";
import { theme, st } from "./constants";
import { useCallback, useState } from "react";
import AppLoading from "expo-app-loading";
import * as NavigationBar from "expo-navigation-bar";
import { GestureHandlerRootView } from "react-native-gesture-handler";

import {
  useFonts,
  SourceSansPro_400Regular,
  SourceSansPro_600SemiBold,
  SourceSansPro_700Bold,
} from "@expo-google-fonts/source-sans-pro";

NavigationBar.setBackgroundColorAsync(theme.colors.APP_BG_COLOR);
NavigationBar.setButtonStyleAsync("light");

function App() {
  const [dayIndex, setDayIndex] = useState<number>(0);
  let [fontsLoaded] = useFonts({
    SourceSansPro_400Regular,
    SourceSansPro_600SemiBold,
    SourceSansPro_700Bold,
  });

  const onSwipe = useCallback(
    (index) => {
      setDayIndex(index);
    },
    [setDayIndex]
  );

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <View style={styles.container}>
      <GestureHandlerRootView style={{ ...StyleSheet.absoluteFillObject }}>
        <Slider dayIndex={dayIndex} onSwipe={onSwipe} />
      </GestureHandlerRootView>
      <Calendar dayIndex={dayIndex} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.APP_BG_COLOR,
    ...st.centerContent,
  },
});

export default registerRootComponent(App);
