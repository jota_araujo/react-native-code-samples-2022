# Project 1: Ticket Booking App

![screencapture](https://bitbucket.org/jota_araujo/react-native-code-samples-2022/raw/d53739fc042cf2f56bdfe73c65ded6a5286d261f/ticket_booking/ticket_booking_concept.gif)

Technologies used:

- Youtube IFrame Player API (via a [wrapper library](https://github.com/LonelyCpp/react-native-youtube-iframe))
- [React Native Reanimated 2.3](https://docs.swmansion.com/react-native-reanimated/)
- [React Native Gesture Handler 2](https://docs.swmansion.com/react-native-gesture-handler/)
- [Expo SDK 44](https://expo.dev/)
- Typescript

[Original concept](https://dribbble.com/shots/16631101-Cinema-App) by Vlad Gorbunov

# Project 2: Habit builder App

![screencapture](https://bitbucket.org/jota_araujo/react-native-code-samples-2022/raw/cbbe85b7803e6e31eafc44bd7f8276404155f459/habit_builder_app/tubik_habit_builder_concept.gif)

Technologies used:

- [React Native Reanimated 2.3](https://docs.swmansion.com/react-native-reanimated/)
- [React Native Gesture Handler](https://docs.swmansion.com/react-native-gesture-handler/)
- [Expo SDK 44](https://expo.dev/)
- Typescript

Design credit: [Tubik on Dribbble](https://dribbble.com/shots/15980950-Habit-Builder-Application)

# Project 3: Crypto Wallet Concept

![screencapture](https://bitbucket.org/jota_araujo/react-native-code-samples-2022/raw/9df962ab6917a0555bde008281ceb88d86843579/crypto_wallet_sample_code/crypto_wallet.gif)

Technologies used:

- [React Native SVG Charts](https://github.com/JesperLekland/react-native-svg-charts)
- [React Native Reanimated 2.3](https://docs.swmansion.com/react-native-reanimated/)
- [React Native Gesture Handler](https://docs.swmansion.com/react-native-gesture-handler/)
- [Expo SDK 44](https://expo.dev/)
- Typescript

[Original design](https://dribbble.com/shots/15963182/) by Dmitry Lauretsky
