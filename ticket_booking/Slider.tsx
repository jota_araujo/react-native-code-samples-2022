import React, { memo, useState } from "react";
import { StyleSheet, Dimensions } from "react-native";
import {
  Gesture,
  GestureDetector,
  Directions,
} from "react-native-gesture-handler";
import Animated, {
  interpolate,
  runOnJS,
  SharedValue,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from "react-native-reanimated";

const { width: screenW } = Dimensions.get("window");

import { CARD_SCALE } from "../constants/";
import MovieCard from "../MovieCard/MovieCard";
import { movies } from "./../../movie_data";

interface SliderProps {
  data: typeof movies;
  expanded: boolean;
  setExpanded: (expanded: boolean) => void;
  selectedIndex: SharedValue<number>;
  zRotation: SharedValue<number>;
}

export default memo(function Slider({
  data,
  expanded,
  setExpanded,
  zRotation,
}: SliderProps) {
  const xPos = useSharedValue(0);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const momentumEndHandler = () => {
    // NOTE: Noop needed due to this issue:  https://github.com/software-mansion/react-native-reanimated/issues/2735
  };
  const scrollHandler = useAnimatedScrollHandler({
    onScroll: (event) => {
      const { x } = event.contentOffset;
      xPos.value = x;
    },
    onMomentumEnd: (event) => {
      const { x } = event.contentOffset;
      runOnJS(setSelectedIndex)(
        Math.abs(Math.round(x / (screenW * CARD_SCALE)))
      );
    },
  });

  // NOTE: Without this hack to  first slider item will not be perfectly centered on the screen
  const sliderOffset = useAnimatedStyle(() => {
    const left = interpolate(
      xPos.value,
      [0, screenW * 0.75],
      [screenW * 0.05, 0],
      { extrapolate: "clamp" }
    );

    return {
      left,
    };
  });

  const flingUp = Gesture.Fling()
    .direction(Directions.UP)
    .onStart((e) => {
      runOnJS(setExpanded)(true);
    });

  const flingDown = Gesture.Fling()
    .direction(Directions.DOWN)
    .onStart((e) => {
      runOnJS(setExpanded)(false);
    });

  return (
    <Animated.View style={[styles.slider, sliderOffset]}>
      <GestureDetector gesture={Gesture.Race(flingUp, flingDown)}>
        <Animated.ScrollView
          scrollEnabled={!expanded}
          onScroll={scrollHandler}
          onMomentumScrollEnd={momentumEndHandler}
          contentContainerStyle={styles.contentContainer}
          scrollEventThrottle={16}
          snapToInterval={screenW * 0.75}
          decelerationRate="fast"
          showsHorizontalScrollIndicator={false}
          horizontal
        >
          {data.map((item, index) => {
            return (
              <MovieCard
                key={index}
                x={xPos}
                index={index}
                expanded={expanded}
                picture={item.picture}
                selectedIndex={selectedIndex}
                movie={movies[index]}
                zRotation={zRotation}
              />
            );
          })}
        </Animated.ScrollView>
      </GestureDetector>
    </Animated.View>
  );
});

const styles = StyleSheet.create({
  slider: {
    position: "absolute",
    top: 0,
    bottom: 150,
    overflow: "visible",
    opacity: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    justifyContent: "center",
    overflow: "visible",
    alignItems: "flex-start",
  },
});
