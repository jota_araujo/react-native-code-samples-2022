import React, { memo, useCallback, useEffect, useState } from "react";
import { Dimensions, Pressable, Text, View, StyleSheet } from "react-native";

import Animated, {
  useAnimatedStyle,
  interpolate,
  withRepeat,
  withTiming,
  Easing,
} from "react-native-reanimated";

import Seat from "../assets/svg/seat2.svg";
import SeatPremium from "../assets/svg/seat_premium.svg";
import { useAnimatedTransform } from "./utils/animation-hooks";
import TimeSelector from "./TimeSelector";

const { width: screenW, height: screenH } = Dimensions.get("window");

const SEAT_COLORS = {
  h: "#252525",
  u: "#252525",
  s: "#555",
  b: "#4671b3",
  p: "#b17e33",
};

const ROW_LETTERS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

const seatArr = [
  ["h", "h", "s", "u", "u", "u", "s", "h", "h"],
  ["s", "s", "s", "s", "s", "s", "s", "s", "u"],
  ["s", "s", "s", "p", "p", "p", "s", "s", "u"],
  ["s", "s", "s", "p", "p", "p", "s", "s", "u"],
  ["b", "b", "b", "s", "s", "s", "s", "s", "u"],
  ["b", "b", "b", "s", "s", "s", "s", "s", "h"],
  ["b", "b", "b", "b", "b", "b", "b", "h", "h"],
];

const SeatType = ({
  color,
  children,
}: {
  color: string;
  children: React.ReactNode;
}) => (
  <View style={{ flexDirection: "row", alignItems: "center" }}>
    <View
      style={{
        backgroundColor: color,
        width: 16,
        height: 16,
        borderRadius: 6,
        marginRight: 4,
      }}
    />
    <Text style={{ fontSize: 12, color: "white" }}>{children}</Text>
  </View>
);

interface SeatViewProps {
  id: string;
  seatStatus: string;
  isSelected: boolean;
  onClick: (key: string) => void;
}

const SeatView = memo(function _SeatView({
  id,
  seatStatus,
  isSelected,
  onClick,
}: SeatViewProps) {
  const { animation: seatBounce, style: seatStyle } = useAnimatedTransform(
    "scale",
    1
  );

  useEffect(() => {
    seatBounce.value = withRepeat(
      withTiming(1.8, { duration: 200, easing: Easing.out(Easing.cubic) }),
      2,
      true
    );
  }, [isSelected]);

  const Comp = seatStatus === "p" ? SeatPremium : Seat;
  return (
    <Pressable key={id} onPress={() => onClick(id)}>
      <Animated.View style={seatStyle}>
        <Comp
          width={30}
          height={30}
          opacity={seatStatus === "h" ? 0 : 1}
          color={
            isSelected
              ? "#6943c2"
              : SEAT_COLORS[seatStatus as keyof typeof SEAT_COLORS]
          }
          viewBox="0 0 381 302"
        />
      </Animated.View>
    </Pressable>
  );
});

interface SeatSelectorProps {
  rotation: Animated.SharedValue<number>;
  setSelectedSeats: Function;
  selectedSeats: string[];
}

const SeatSelector = memo(function _SeatSelector({
  rotation,
  setSelectedSeats,
  selectedSeats,
}: SeatSelectorProps) {
  const onClickSeat = useCallback((key) => {
    setSelectedSeats((prev: string[]) => {
      if (prev.includes(key)) {
        return prev.filter((s) => s !== key);
      } else {
        return [...prev, key];
      }
    });
  }, []);

  const animatedProps = useAnimatedStyle(() => {
    const height = interpolate(rotation.value, [0, 70], [700, 390]);
    const top = interpolate(
      rotation.value,
      [0, 30, 72],
      [screenH + 150, 530, 380]
    );
    const opacity = interpolate(rotation.value, [0, 30, 70], [0, 0, 1]);
    return {
      height,
      top,
      opacity,
    };
  });

  return (
    <Animated.View style={[styles.container, animatedProps]}>
      {seatArr.map((row, i) => (
        <Animated.View key={i} style={styles.seat}>
          {row.map((seatStatus, j) => {
            const key = [ROW_LETTERS[i], j + 1].join("");
            return (
              <SeatView
                key={key}
                id={key}
                isSelected={selectedSeats.includes(key)}
                seatStatus={seatStatus}
                onClick={onClickSeat}
              />
            );
          })}
        </Animated.View>
      ))}
      <TimeSelector />
    </Animated.View>
  );
});

export default SeatSelector;

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    justifyContent: "space-around",
    top: 330,
    right: 30,
    left: 30,
  },
  seat: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 16,
    justifyContent: "space-between",
  },
});
